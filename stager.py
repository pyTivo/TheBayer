# Module:       stager.py
# Author:       Eric von Bayer
# Contact:      
# Date:         August 18, 2009
# Description:
#     Module to handle deferred callbacks in stages.  
#
# Copyright (c) 2009, Eric von Bayer
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#    * The names of the contributors may not be used to endorse or promote
#      products derived from this software without specific prior written
#      permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Class to hold a stage
class Stage(object):
    """An individual stage to be executed at a later time"""
    
    # Initialize the stage
    def __init__(self):
        self.__callbacks = list()
        self.__done = False
    
    # Execute all the callbacks in the stage   
    def Execute(self):
        if self.__done == False:
            self.__done = True
            for call in self.__callbacks:
                call()
            del self.__callbacks
    
    # Add a callback to the stage        
    def AddCallback(self, call):
        if self.__done == True:
            call()
        else:
            self.__callbacks.append( call )

# Internal storage for the stages
stager_stages = dict()

# Execute the named stage
def Execute(name):
    if name in stager_stages:
        stager_stages[name].Execute()

# Add a callback to the named stage
def AddCallback(name, call):
    if not name in stager_stages:
        stager_stages[name] = Stage()
        
    stager_stages[name].AddCallback( call )
