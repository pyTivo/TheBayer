import cookielib
import logging
import os
import re
import thread
import time
import urllib2
import urlparse
import stager
from urllib import quote, unquote
from xml.dom import minidom
from xml.sax.saxutils import escape

from Cheetah.Template import Template

import config
from metadata import tag_data, from_container
from plugin import EncodeUnicode, Plugin

SCRIPTDIR = os.path.dirname(__file__)

CLASS_NAME = 'ToGo'

# Some error/status message templates

MISSING = """<h3>Missing Data.</h3>  <br>
You must set both "tivo_mak" and "togo_path" before using this 
function.<br>
The <a href="%s">ToGo</a> page 
will reload in 10 seconds."""

TRANS_INIT = """<h3>Transfer Initiated.</h3>  <br>
Your selected transfer has been initiated.<br>
The <a href="%s">ToGo</a> page 
will reload in 3 seconds."""

TRANS_STOP = """<h3>Transfer Stopped.</h3>  <br>
Your transfer has been stopped.<br>
The <a href="%s">ToGo</a> page 
will reload in 3 seconds."""

UNABLE = """<h3>Unable to Connect to TiVo.</h3>  <br>
pyTivo was unable to connect to the TiVo at %s</br>
This most likely caused by an incorrect Media Access Key.  Please return 
to the ToGo page and double check your Media Access Key.<br>
The <a href="%s">ToGo</a> page will
reload in 20 seconds."""

# Get the logger
logger = logging.getLogger('pyTivo.togo')

# Preload the templates
trname = os.path.join(SCRIPTDIR, 'templates', 'redirect.tmpl')
tnname = os.path.join(SCRIPTDIR, 'templates', 'npl.tmpl')
REDIRECT_TEMPLATE = file(trname, 'rb').read()
NPL_TEMPLATE = file(tnname, 'rb').read()

# Precompile the regular expressions
RE_QUEUE_FILE = re.compile( r".* - ([0-9]+)\..*\.queue")

status = {} # Global variable to control download threads
tivo_cache = {} # Cache of TiVo NPL
file_queue = [] # Queue of files to get
queue_running = False # Flag to know if the queue is running
tivo_opener = None # Default opener

# After the scan is done, setup the queue
stager.AddCallback( "post_scan", lambda: thread.start_new_thread(setup_queue, \
    tuple() ) )

# Get the Tivo ToGo path either directly or from the share
def GetTogoPath():
    togo_path = config.get_server('togo_path')

    for name, data in config.getShares():
        if togo_path == name:
            togo_path = data.get('path')
            break
            
    return togo_path

# Read in the program details for a given ID
def get_program_details( pid, ip = None, tsn = None ):
    global tivo_opener
    
    tivo_mak = config.get_server('tivo_mak')
    
    # If the IP is given, just go after our data
    if ip != None:
        tivoIP = ip
    
    # If the tsn is given then lookup the proper tivo
    elif tsn != None:
        if tsn in config.tivos:
            tivoIP = config.tivos[tsn]
        else:
            return None
    
    # Iterate through all the tivos looking for the ID
    else:
        for tsn in config.tivos:
            rv = get_program_details( pid, tsn=tsn )
            if rv != None:
                return rv
        return None

    theurl = ('https://' + tivoIP + '/TiVoConnect?' \
        'Command=QueryContainer&Container=/NowPlaying/' + str(pid) )
     
    #print "Getting details:", theurl
    
    # Get the details
    if (theurl not in tivo_cache or
        (time.time() - tivo_cache[theurl]['thepage_time']) >= 60):
        
        # if page is not cached or old then retreive it
        try:
            handle = get_html_req(theurl)
        except IOError, e:
            return None
            
        tivo_cache[theurl] = {'thepage': minidom.parse(handle),
                              'thepage_time': time.time()}
        handle.close()

    # Get the xml
    xmldoc = tivo_cache[theurl]['thepage']
    #print xmldoc.toxml()
    
    items = xmldoc.getElementsByTagName('Item')
    url_end = "&id="+str(pid)
    found = False
    
    # Even though we asked for one entry we may actually get more if it's in
    # a folder.  So look for the right one.
    for item in items:
    
        # Container for the entry
        entry = dict()
        entry['Url'] = tag_data( item, 'Links/Content/Url' )

        if entry['Url'][-len(url_end):] == url_end:
        
            # Read in the bits we care about
            keys = {'Title': 'Details/Title',
                    'Icon': 'Links/CustomIcon/Url',
                    'SourceSize': 'Details/SourceSize',
                    'Duration': 'Details/Duration',
                    'CaptureDate': 'Details/CaptureDate',
                    'CopyProtected': 'CopyProtected',
                    'UniqueId': 'UniqueId'}
            for key in keys:
                value = tag_data(item, keys[key])
                if value:
                    entry[key] = value
            
            return entry
            
    return None

def setup_queue():
    global tivo_opener
    
    tivo_mak = config.get_server('tivo_mak')
    togo_path = GetTogoPath()
    
    # Setup a cookie jar and the password/handler for the TiVo
    cjar = cookielib.LWPCookieJar()
    auth_handler = urllib2.HTTPDigestAuthHandler()
    for tsn in config.tivos:
        auth_handler.add_password('TiVo DVR', config.tivos[tsn], 'tivo', \
            tivo_mak)
        auth_handler.add_password('TiVo DVR', config.tivos[tsn]+":80", 'tivo', \
            tivo_mak)
        auth_handler.add_password('TiVo DVR', config.tivos[tsn]+":443", 'tivo', \
            tivo_mak)
    tivo_opener = urllib2.build_opener( urllib2.HTTPCookieProcessor(cjar), \
        auth_handler)

    # Walk through the queue files to find any files needed
    for fn in os.listdir( togo_path ):
        try:
            rd = RE_QUEUE_FILE.match( fn )
            if rd:
                # Get the details for the program ID
                entry = get_program_details( int(rd.group(1)) )
                
                # If there is no data, leave the file, but don't try to get it
                # it may exist sometime later
                if entry:
                    logger.info( "Queueing " + entry['Url'] )
                    queue_tivo_file( entry['Url'], no_process=True )
                else:
                    logger.warning( "Didn't find source for "+fn+ \
                        ", ignoring." )
        except:
            logger.error( "Problem queueing "+fn )
            pass
    
    # Process the built up queue
    process_queue()                

# Queue a tivo file
def queue_tivo_file(url, no_process=False):
    global queue_running, file_queue, status

    # Build the queue files as a way to know what remains if we restart
    parse_url = urlparse.urlparse(url)
    name = unquote(parse_url[2])[10:].split('.')
    name.insert(-1," - " + unquote(parse_url[4]).split("id=")[1] + ".")
    name.append(".queue")
    qfile = os.path.join( GetTogoPath(), "".join(name))
    open(qfile, "w").close()
    
    # Add the file to the queue
    status[url] = { 'running': True, 'queued': True, 'error': '', \
        'rate': '', 'size': 0, 'finished': False}
    file_queue.append( url )
    
    # Kickoff processing the queue
    if not no_process:
        process_queue()

# Process the queue, downloading all the files
def process_queue():
    global queue_running, file_queue

    # If the queue isn't running, then process the queue
    if queue_running == False:
    
        # We should only have one, so prevent a second from starting
        queue_running = True
        
        # While there are entries on the queue, process the next one
        while len( file_queue ):
        
            # Get the next entry off the queue
            url = file_queue.pop(0)
            
            # Get the file
            get_tivo_file( url )
            
            #time.sleep(5)
            
        # End the queue
        queue_running = False

def get_html_req(url, headers=None):
    global tivo_opener

    parse_url = urlparse.urlparse(url)
    
    # Build up the request
    req = urllib2.Request(url)
    if isinstance( headers, dict ):
        for key,val in headers.items():
            req.add_header( key, val )

    # Loop just in case we get a server busy message
    while 1:
        try:
            # Open the URL using our authentication/cooky opener
            return tivo_opener.open(req)
            
        # Do a retry if the TiVo responds that the server is busy
        except urllib2.HTTPError, e:
            if e.code == 503:
                time.sleep(5)
                continue
                
            # Throw the error otherwise 
            raise

def get_tivo_file(url):
    global status, tivo_opener

    if not status[url]['queued']:
        return
    
    status[url]['queued'] = False

    togo_path = GetTogoPath()    
    parse_url = urlparse.urlparse(url)

    name = unquote(parse_url[2])[10:].split('.')
    name.insert(-1," - " + unquote(parse_url[4]).split("id=")[1] + ".")
    outfile = os.path.join(togo_path, "".join(name))
    server = parse_url[1].split(":",1)[0]

    name.append( ".queue" )
    qfile = os.path.join(togo_path, "".join(name))


    try:
        handle = get_html_req(url)    
        f = open(outfile, 'wb')

    #except urllib2.HTTPError, e:
    #    raise
    except IOError, e:
        status[url]['queued'] = False
        status[url]['running'] = False
        status[url]['error'] = e.code
        return

    length = 0
    start_time = time.time()
    try:
        while status[url]['running']:
            output = handle.read(1024000)
            if not output:
                break
            length += len(output)
            f.write(output)
            now = time.time()
            elapsed = now - start_time
            if elapsed >= 5:
                status[url]['rate'] = int(length / elapsed) / 1024
                status[url]['size'] += length
                length = 0
                start_time = now
        if status[url]['running']:
            status[url]['finished'] = True
    except Exception, msg:
        logging.getLogger('pyTivo.togo').info(msg)
    handle.close()
    
    # If we were aborted, delete the partial file as we can't actually resume it
    if not status[url]['running']:
        os.remove(outfile)
    
    status[url]['running'] = False
    f.close()
    os.remove(qfile)

class ToGo(Plugin):
    CONTENT_TYPE = 'text/html'

    def NPL(self, handler, query):
        global tivo_opener
        
        #print handler.path
        
        shows_per_page = 50 # Change this to alter the number of shows returned
        cname = query['Container'][0].split('/')[0]
        folder = ''
        tivo_mak = config.get_server('tivo_mak')
        togo_path = GetTogoPath()

        if 'TiVo' in query:
            tivoIP = query['TiVo'][0]
            theurl = ('https://' + tivoIP +
                      '/TiVoConnect?Command=QueryContainer&ItemCount=' +
                      str(shows_per_page) + '&Container=/NowPlaying')
            if 'Folder' in query:
                folder += query['Folder'][0]
                theurl += '/' + folder
            if 'AnchorItem' in query:
                theurl += '&AnchorItem=' + quote(query['AnchorItem'][0])
            if 'AnchorOffset' in query:
                theurl += '&AnchorOffset=' + query['AnchorOffset'][0]

            if (theurl not in tivo_cache or
                (time.time() - tivo_cache[theurl]['thepage_time']) >= 60):
                # if page is not cached or old then retreive it
                try:
                    page = get_html_req(theurl)
                except IOError, e:
                    t = Template(REDIRECT_TEMPLATE)
                    t.time = '20'
                    t.url = '/TiVoConnect?Command=NPL&Container=' + quote(cname)
                    t.text = UNABLE % (tivoIP, quote(cname))
                    handler.send_response(200)
                    handler.send_header('Content-Type', 'text/html')
                    handler.end_headers()
                    handler.wfile.write(t)
                    return
                tivo_cache[theurl] = {'thepage': minidom.parse(page),
                                      'thepage_time': time.time()}
                page.close()

            xmldoc = tivo_cache[theurl]['thepage']
            items = xmldoc.getElementsByTagName('Item')
            TotalItems = tag_data(xmldoc, 'Details/TotalItems')
            ItemStart = tag_data(xmldoc, 'ItemStart')
            ItemCount = tag_data(xmldoc, 'ItemCount')
            FirstAnchor = tag_data(items[0], 'Links/Content/Url')

            data = []
            for item in items:
                entry = {}
                entry['ContentType'] = tag_data(item, 'ContentType')
                for tag in ('CopyProtected', 'UniqueId'):
                    value = tag_data(item, tag)
                    if value:
                        entry[tag] = value
                if entry['ContentType'] == 'x-tivo-container/folder':
                    entry['Title'] = tag_data(item, 'Title')
                    entry['TotalItems'] = tag_data(item, 'TotalItems')
                    lc = int(tag_data(item, 'LastChangeDate'), 16)
                    entry['LastChangeDate'] = time.strftime('%b %d, %Y',
                                                            time.localtime(lc))
                else:
                    entry.update(from_container(item))
                    keys = {'Icon': 'Links/CustomIcon/Url',
                            'Url': 'Links/Content/Url',
                            'SourceSize': 'Details/SourceSize',
                            'Duration': 'Details/Duration',
                            'CaptureDate': 'Details/CaptureDate'}
                    for key in keys:
                        value = tag_data(item, keys[key])
                        if value:
                            entry[key] = value

                    entry['SourceSizeText'] = ( '%.3f GB' %
                        (float(entry['SourceSize']) / (1024 ** 3)) )

                    dur = int(entry['Duration']) / 1000
                    entry['DurationText'] = ( '%02d:%02d:%02d' %
                        (dur / 3600, (dur % 3600) / 60, dur % 60) )

                    entry['CaptureDateText'] = time.strftime('%b %d, %Y',
                        time.localtime(int(entry['CaptureDate'], 16)))

                data.append(entry)
        else:
            data = []
            tivoIP = ''
            TotalItems = 0
            ItemStart = 0
            ItemCount = 0
            FirstAnchor = ''

        cname = query['Container'][0].split('/')[0]
        t = Template(NPL_TEMPLATE, filter=EncodeUnicode)
        t.escape = escape
        t.quote = quote
        t.folder = folder
        t.status = status
        t.tivo_mak = tivo_mak
        t.togo_path = togo_path
        t.tivos = config.tivos
        t.tivo_names = config.tivo_names
        t.tivoIP = tivoIP
        t.container = cname
        t.data = data
        t.len = len
        t.TotalItems = int(TotalItems)
        t.ItemStart = int(ItemStart)
        t.ItemCount = int(ItemCount)
        t.FirstAnchor = quote(FirstAnchor)
        t.shows_per_page = shows_per_page
        t.my_url = handler.path
        handler.send_response(200)
        handler.send_header('Content-Type', 'text/html')
        handler.end_headers()
        handler.wfile.write(t)

    def ToGo(self, handler, query):
        cname = query['Container'][0].split('/')[0]
        tivoIP = query['TiVo'][0]
        tivo_mak = config.get_server('tivo_mak')
        togo_path = GetTogoPath()
        t = Template(REDIRECT_TEMPLATE)
        t.url = query['Redirect'][0]
        if tivo_mak and togo_path:
            theurl = query['Url'][0]
            thread.start_new_thread( queue_tivo_file, (theurl,) )
            t.time = '3'
            t.text = TRANS_INIT % t.url
        else:
            t.time = '10'
            t.text = MISSING % t.url
        handler.send_response(200)
        handler.send_header('Content-Type', 'text/html')
        handler.end_headers()
        handler.wfile.write(t)

    def ToGoStop(self, handler, query):
        theurl = query['Url'][0]
        status[theurl]['queued'] = False
        status[theurl]['running'] = False
        try:
            file_queue.remove(theurl)
        except:
            pass

        cname = query['Container'][0].split('/')[0]
        tivoIP = query['TiVo'][0]
        t = Template(REDIRECT_TEMPLATE)
        t.time = '3'
        t.url = query['Redirect'][0]
        t.text = TRANS_STOP % (t.url)
        handler.send_response(200)
        handler.send_header('Content-Type', 'text/html')
        handler.end_headers()
        handler.wfile.write(t)
